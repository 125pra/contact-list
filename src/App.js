import React, { Component } from "react";
import "./App.css";
import contactData from "../src/data/contact.json";
import AddContactOrEdit from "./components/addContact";
import ShowContact from "./components/showContactList";
class App extends Component {
  state = {
    contactData,
    addState: false,
    searchWord: ""
  };

  handleDelete = singleDataId => {
    this.setState({
      contactData: this.state.contactData.filter(
        contact => contact.id !== singleDataId
      )
    });
  };

  handleSearchChange = singleContactData => {
    let searchWord = singleContactData.target.value;
    this.setState({ searchWord });
  };

  callToToggleAddState = singleContactData => {
    this.setState(prevState => {
      return {
        addState: !prevState.addState,
        singleDataId: singleContactData.id
      };
    });
  };

  addContact = singleContactData => {
    this.setState({
      contactData: [...this.state.contactData, singleContactData],
      addState: !this.state.addState
    });
  };

  editContact = someContact => {
    this.setState({
      contactData: this.state.contactData.map(singleData => {
        if (singleData.id === someContact.id) {
          return { ...someContact };
        }
        return singleData;
      }),
      addState: !this.state.addState
    });
  };

  handleSearch = () => {
    const contactData = this.state.contactData.filter(
      contact =>
        contact.first_name
          .toLowerCase()
          .includes(this.state.searchWord.toLowerCase()) ||
        contact.last_name
          .toLowerCase()
          .includes(this.state.searchWord.toLowerCase()) ||
        contact.email
          .toLowerCase()
          .includes(this.state.searchWord.toLowerCase())
    );
    return contactData;
  };

  render() {
    const contactData = this.handleSearch();
    const singleDataId = this.state.singleDataId;
    return (
      <React.Fragment>
        <div className="container">
          <button
            className="btn btn-success"
            onClick={this.callToToggleAddState}
          >
            Add New Contact
          </button>
          {this.state.addState && !this.state.singleDataId ? (
            <AddContactOrEdit
              singleData={[
                {
                  id:"",
                  avatar_url:"",
                  first_name: "",
                  last_name: "",
                  email: "",
                  phone: "",
                  isAdd:true
                }]
              }
              contactData={this.state.contactData}
              onClose={this.callToToggleAddState}
              onAdd={this.addContact}
            />
          ) : null}

          {this.state.addState && this.state.singleDataId ? (
            <AddContactOrEdit
              singleData={contactData.filter(
                contact => contact.id === singleDataId
              )}
              onClose={this.callToToggleAddState}
              onEdit={this.editContact}
            />
          ) : null}

          <h3 className="contactList">Prakash-Contact-List</h3>
          <input
            onChange={this.handleSearchChange}
            className="form-control input-normal"
            type="text"
            placeholder="Search"
            aria-label="Search"
          />
          <table className="table table-hover">
            <thead className="thead">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Img</th>
                <th scope="col">First</th>
                <th scope="col">Last</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              {contactData.map(singleData => (
                <ShowContact
                  key={singleData.id}
                  onDelete={this.handleDelete}
                  singleData={singleData}
                  onClick={this.callToToggleAddState}
                />
              ))}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
