import React, { Component } from "react";

class AddContactOrEdit extends Component {
  state = {
    id:this.props.singleData[0].id,
    avatar_url:this.props.singleData[0].avatar_url,
    first_name: this.props.singleData[0].first_name,
    last_name: this.props.singleData[0].last_name,
    email: this.props.singleData[0].email,
    phone: this.props.singleData[0].phone,
    isAdd:this.props.singleData[0].isAdd,
    contactData:this.props.contactData
  };

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleSubmit = event => {
    if(this.props.singleData[0].isAdd)
    { 
      const singleContactData = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
      phone: this.state.phone
    };
    let arrayOfId = this.state.contactData.map(singleData => singleData.id);
      let maxId = Math.max(...arrayOfId);
      if (maxId === -Infinity) {
        maxId = 0;
      }
      const tempImage = this.state.first_name;
      singleContactData["id"] = maxId + 1;
      singleContactData["avatar_url"] =
      `https://joeschmoe.io/api/v1/${tempImage}` 
    this.props.onAdd(singleContactData);
  }
   
    else{
      const singleContactData = {
     id:this.state.id,
     avatar_url:this.state.avatar_url,
     first_name: this.state.first_name,
     last_name: this.state.last_name,
     email: this.state.email,
     phone: this.state.phone
    }
    this.props.onEdit(singleContactData);  
  };
}

  render() {
    return (
      <React.Fragment>
        <div className="modall" tabIndex="-1" role="dialog">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Add New Contact Or Edit Contact</h5>
              </div>
              <div className="modal-body">
                <form onSubmit={this.handleSubmit} className="contact">
                  <div id="form">
                    <input
                      className="input m-2"
                      name="first_name"
                      type="text"
                      placeholder="first name"
                      onChange={this.handleChange}
                      value={this.state.first_name}
                    />
                    <input
                      className="input m-2"
                      name="last_name"
                      type="text"
                      placeholder="last name"
                      value={this.state.last_name}
                      onChange={this.handleChange}
                    />
                    <input
                      className="input m-2"
                      name="email"
                      type="email"
                      placeholder="email"
                      value={this.state.email}
                      onChange={this.handleChange}
                    />
                    <input
                      className="input m-2"
                      name="phone"
                      type="text"
                      placeholder="phone no"
                      value={this.state.phone}
                      onChange={this.handleChange}
                    />
                    <input
                      type="submit"
                      className="btn btn-info m-2"
                      value="Submit "
                    />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  onClick={this.props.onClose}
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AddContactOrEdit;
