import React, { Component } from "react";
class ShowContact extends Component {
  render() {
    console.log(this.props.singleData)
    return (
      <React.Fragment>
        <tr className="singleRow">
          <td>{this.props.singleData.id}</td>
          <td>
            <img src={this.props.singleData.avatar_url} />
          </td>
          <td>{this.props.singleData.first_name}</td>
          <td>{this.props.singleData.last_name}</td>  
          <td>{this.props.singleData.email}</td>
          <td>{this.props.singleData.phone}</td>
          <td>
            <span>
              <button className="btn btn-secondary btn-sm" onClick={() => this.props.onClick(this.props.singleData) }>Edit</button>
              <button
                onClick={() => this.props.onDelete(this.props.singleData.id)}
                className="btn btn-danger btn-sm m-2"
              >
                Delete
              </button>
            </span>
          </td>
        </tr>
      </React.Fragment>
    );
  }
}

export default ShowContact;




